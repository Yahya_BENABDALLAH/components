import React from 'react'
import Warning from '../img/Warning.svg'
import "./style/style.css";

const Erreur = () => {
    return (
      <div className="Warning">
        <img src={Warning} alt="Warning"></img>
      </div>
    );
}

export default Erreur
