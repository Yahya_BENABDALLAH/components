import React from "react";
import MaterialTable from "material-table";
import "./style/style.css";

const columns = [
  {
    field: "name",
    title: "Name",
    hiddenByColumnsButton: "true",
    width: "10% !important",
    cellStyle: {
      backgroundColor: "#e6e6ff",
      textAlign: "center",
    },
  },
  { field: "capital", title: "Capital", width: 130 },
  { field: "subregion", title: "Region", width: 130 },
  { field: "population", title: "Population", width: 130 },
  { field: "nativeName", title: "N-Name", width: 130 },
  { field: "alpha3Code", title: "Code", width: 130 },
  { field: "borders", title: "borders", width: 130 },
];

const MaterialTables = ({ data }) => {
  
  return (
    <div className="tableAttente">
      <MaterialTable
        title="Pays"
        data={data}
        columns={columns}
        localization={{
          body: {
            emptyDataSourceMessage: "Aucune données à afficher.",
          },
        }}
        style={{
          width: "100%",
          boxShadow: "0px 0px 15px 6px rgba(0,0,0,0.1)",
          borderRadius: "20px",
        }}
        options={{
          draggable: false,
          headerStyle: {
            backgroundColor: "#1026AA",
            color: "#ffffff",
            textAlign: "center",
            fontWeight: "bold",
            position: "sticky",
          },
          maxBodyHeight: "300px",
          minBodyHeight: "300px",
          search: true,
          paging: true,
          exportButton: true,
          exportAllData: true,
        }}
      />
    </div>
  );
};

export default MaterialTables;
