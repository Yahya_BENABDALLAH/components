import React from "react";
import { DataGrid } from "@material-ui/data-grid";
import "./style/style.css";
  const columns = [
    { field: "name", headerName: "Name", width: 130 },
    { field: "capital", headerName: "Capital", width: 130 },
    { field: "subregion", headerName: "Region", width: 130 },
    { field: "population", headerName: "Population", width: 130 },
    { field: "nativeName", headerName: "N-Name", width: 130 },
    { field: "alpha3Code", headerName: "Code", width: 130 },
    { field: "borders", headerName: "borders", width: 130 },
  ];

const Grid = ({data}) => {

  return (
    <>
        <div>
          <div style={{ height: 400, width: "100%" }}>
            <DataGrid
              rows={data}
              columns={columns}
              density="compact"
              pageSize={50}
              getRowId={(r)=> r.name}
            />
          </div>
        </div>
    </>
  );
};

export default Grid;
