import React  from "react";
import Grid from "./Grid/Grid";
import { useQuery } from "react-query";
import { getCountry } from "../../tools/axios";
import Loading from "../Loading/Loading";
import TableUI from "./TableUI/TableUI";
import MaterialTables from "./MaterialTables/MaterialTables";

const Tables = () => {

  const Countries = useQuery("Countries", async () => {
    return await getCountry().then((e) => e);
  }, 
//   {
//       refetchInterval:10000,
//   }
  );

  return (
    <>
      {Countries.isFetched === true ? (
        <div className="Tables">
           <div >
            Tables Grid
            <Grid data={Countries.data.data} />
          </div>
           <div>
            Tables Material UI
            <TableUI data={Countries.data.data} />
          </div>
          <div>
            Tables Material Table
            <MaterialTables data={Countries.data.data} />
          </div> 
        </div>
      ) : (
        <Loading />
      )}
    </>
  );
};

export default Tables;
