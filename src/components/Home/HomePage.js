import React from 'react'
import { Link } from 'react-router-dom';
import Home from "../img/Home.svg";
import "./style/style.css";

const HomePage = () => {
    return (
      <div className="Home">
        <img src={Home} alt="Home"></img>
        <Link to="/Tables" >
        Test
        </Link >
      </div>
    );
}

export default HomePage
