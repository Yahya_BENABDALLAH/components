import React from 'react'
import {Switch,Route} from 'react-router-dom'
import Erreur from '../Erreur/Erreur';
import HomePage from '../Home/HomePage';
import Login from '../Login/Login';
import Tables from '../Tables/Tables';
import NavBar from "../NavBar/NavBar";
import Loading from '../Loading/Loading';

const LaRoute = () => {
    return (
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route exact path="/Erreur" component={Erreur} />
        <Route exact path="/Login" component={Login} />
        <Route exact path="/NavBar" component={NavBar} />
        <Route exact path="/Tables" component={Tables} />
        <Route exact path="/Loading" component={Loading} />
      </Switch>
    );
}

export default LaRoute
