import React, { useEffect, useState } from "react";
import { Button, Form } from "react-bootstrap";
import { Redirect } from "react-router";
import Logins from "../img/Login.svg";
import "./style/style.css";

const Login = () => {
  const [mail, setMail] = useState("");
  const [password, setPassword] = useState("");
  const [loged, setLoged] = useState(false);

  const seConnecter = (e) => {
    e.preventDefault();
    console.log({ mail, password });
    localStorage.setItem(mail, password);
    setLoged(true);
  };
  useEffect(() => {
    if (localStorage.getItem > 0) {
      setLoged(true);
    }
  }, [loged]);

  if (loged) {
    return <Redirect to="/Home" />;
  }

  return (
    <>
      <img className="Login"  src={Logins} alt="Login"></img>
      <div className="loginForm">
        <Form className="form" onSubmit={seConnecter}>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter email"
              onChange={(e) => setMail(e.target.value)}
            />
          </Form.Group>

          <Form.Group controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Password"
              onChange={(e) => setPassword(e.target.value)}
            />
          </Form.Group>

          <Button variant="primary" type="submit">
            Se connecter
          </Button>
        </Form>
      </div>
    </>
  );
};

export default Login;
