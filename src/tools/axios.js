import axios from "axios";

const country = axios.create({
  baseURL: "https://restcountries.eu/rest/v2/all",
  delayed: true,
});


async function getCountry() {
  return await country({
    method: "get",
  });
}
export { getCountry };
